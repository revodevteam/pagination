<?php
namespace Framework\Pagination;

use Generator;

class PaginationBag
{

    /**
     *
     * @var \Framework\Pagination\Page
     */
    public $firstPage;

    /**
     *
     * @var \Framework\Pagination\Page
     */
    public $lastPage;

    /**
     *
     * @var \Framework\Pagination\Page
     */
    public $prePage;

    /**
     *
     * @var \Framework\Pagination\Page
     */
    public $nextPage;

    /**
     *
     * @var \Framework\Pagination\Page[]
     */
    public $links;

    public function __construct(Page $firstPage, Page $lastPage, Page $prePage, Page $nextPage, \Iterator $links)
    {
        $this->firstPage = $firstPage;
        $this->lastPage = $lastPage;
        $this->prePage = $prePage;
        $this->nextPage = $nextPage;
        $this->links = $links;
    }
}
